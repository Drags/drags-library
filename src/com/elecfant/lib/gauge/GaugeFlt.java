package com.elecfant.lib.gauge;

public class GaugeFlt extends Gauge<Float> {

	public GaugeFlt(Float maximum) {
		super(maximum);
	}

	@Override
	protected void validateCurrentValue() {
		
		assert(m_minimum <= m_maximum);
		
		if (m_current > m_maximum) {
			m_current = m_maximum;
			return;
		} else if (m_current < m_minimum) {
			m_current = m_minimum;
			return;
		}
	}

	@Override
	public Float getRange() {
		return m_maximum - m_minimum;
	}
	
	protected void initialize() {
		m_minimum = 0.0f;
		m_maximum = 100.0f;
		m_current = 0.0f;
		m_step = 1.0f;
	}

}
