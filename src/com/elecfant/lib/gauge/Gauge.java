package com.elecfant.lib.gauge;

public abstract class Gauge<T extends Number> {
	protected T m_minimum;
	protected T m_maximum;
	protected T m_step;
	protected T m_current;
	
	public Gauge(T maximum) {
		initialize();
	}

	protected abstract void initialize();

	public void setMinimum(T minimum) {
		m_minimum = minimum;
		validateCurrentValue();
	}
	
	public void setMaximum(T maximum) {
		m_maximum = maximum;
		validateCurrentValue();
	}
	
	public void setCurrent(T current) {
		m_current = current;
		validateCurrentValue();
	}
	
	public void setStep(T step) {
		m_step = step;
	}
	
	public T getCurrent() {
		return m_current;
	}

	protected abstract void validateCurrentValue();
	
	public abstract T getRange();
	
	public float getPercentF() {
		return (m_current.floatValue() - m_minimum.floatValue())/getRange().floatValue();
	}
	
	public double getPercentD() {
		return (m_current.doubleValue() - m_minimum.doubleValue())/getRange().doubleValue();
	}
	
	public long getCurrentL() {
		return  m_current.longValue();
	}
	public int getCurrentI() {
		return m_current.intValue();
	}
}
