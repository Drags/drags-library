package com.elecfant.lib.gauge;

public class GaugeInt extends Gauge<Integer> {

	protected void initialize() {
		m_minimum = 0;
		m_maximum = 100;
		m_step = 1;
		m_current = 0;
	}
	
	public GaugeInt(final int maximum) {
		super(maximum);
	}
	
	@Override
	public Integer getRange() {
		return m_maximum - m_minimum;
	}

	@Override
	protected void validateCurrentValue() {
		assert( m_minimum <= m_maximum);
		
		if (m_current < m_minimum) {
			m_current = m_minimum;
			return;
		} else if (m_current > m_maximum) {
			m_current = m_maximum;
			return;
		}
	}
}
