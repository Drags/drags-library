package com.elecfant.lib;

import java.util.LinkedList;

public class MovingAverage {
	private LinkedList<Double> list;
	private int max_count = 0;
	double sum = 0.0;
	public MovingAverage(int count) {
		
		list = new LinkedList<Double>();
		max_count = count;
		assert(max_count > 0);
	}
	
	public void push(double newValue) {
		if (list.size() == max_count) {
			sum -= list.removeFirst().doubleValue();
		}
		sum += newValue;
		list.add(new Double(newValue));
	}
	
	public double average() {
		if (list.size() == 0) return 0.0;
		return sum / list.size();
	}
}
