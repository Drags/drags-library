package com.elecfant.lib.logger;

public interface LogReceiver {
	public void log(String message, int priority);
	public boolean isReady();
	public void activate();
	public void deactivate();
}
