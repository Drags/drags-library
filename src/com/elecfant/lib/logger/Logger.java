package com.elecfant.lib.logger;

import java.util.Vector;

public class Logger {

	/**
	 * Default LogReceiver implementation. Outputs all messages to System.out. If priority of the message is above zero, it is printed to System.err.
	 * @author Drags
	 *
	 */
	protected static class DefaultLogReceiver implements LogReceiver {

		@Override
		public void log( String message, int priority) {
			if (priority > 0)
				System.err.println(message);
			else
				System.out.println(message);
		}

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void activate() {
		}

		@Override
		public void deactivate() {
		}

	}

	// TODO maybe switch to short or byte? integer is at least 2 times too much than logging needs
	public static final int DEFAULT_PRIORITY = 0;
	public static final int HIGHEST_PRIORITY = Integer.MAX_VALUE;
	public static final int LOWEST_PRIORITY = Integer.MIN_VALUE;

	protected static Vector<LogReceiver> m_receivers;

	protected static LogReceiver defaultLogger;
	
	protected static boolean initialized = false;
	
	public static void init(boolean enableDefault) {
		if (initialized) {
			logMessage("Logger is already initialized!", 1);
			return;
		}
		m_receivers = new Vector<LogReceiver>();
		if (enableDefault) {
			defaultLogger = new DefaultLogReceiver();
			m_receivers.add(defaultLogger);
		}
		initialized = true;
		logMessage("Logger initialized.", 0);
	}
	
	public static void init() {
		init(true);
	}

	public static void registerLogReceiver(LogReceiver receiver) {
		if (!initialized) {
			init();
		}
		if (!m_receivers.contains(receiver)) {
			m_receivers.add(receiver);
		}
	}

	public static void logMessage( String message, int priority) {
		if (!initialized) {
			init();
		}
		for (LogReceiver receiver : m_receivers) {
			receiver.log( message, priority);
		}
	}
	
	public static void l(String message) {
		logMessage( message, DEFAULT_PRIORITY);
	}
	public static void v( String message) {
		logMessage( message, LOWEST_PRIORITY);
	}
	
	public static void e( String message) {
		logMessage( message, HIGHEST_PRIORITY);
	}
}
