/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.lib;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to supply with working examples of all the classes in this
 * library.
 * 
 * @author Drags
 */
public class Examples {

	public static void main(String[] args) {
		int result = 0;

		while (result >= 0) {
			byte[] input = new byte[20];
			System.out.println("Select method to call to show an example. -1 to quit.");
			Method[] methods = Examples.class.getMethods();
			for (int i = 0; i < methods.length; ++i) {
				System.out.println(i + " " + methods[i].getName());
			}
			try {
				System.in.read(input);
			} catch (IOException ex) {
				Logger.getLogger(Examples.class.getName()).log(Level.SEVERE, null, ex);
			}
			String string = new String(input);
			string = string.trim();
			result = Integer.decode(string);
			if (result >= 0 && result < methods.length) {
				try {
					methods[result].invoke((Object)null, (Object)null);
				} catch (IllegalAccessException ex) {
					Logger.getLogger(Examples.class.getName()).log(Level.SEVERE, null, ex);
				} catch (IllegalArgumentException ex) {
					Logger.getLogger(Examples.class.getName()).log(Level.SEVERE, null, ex);
				} catch (InvocationTargetException ex) {
					Logger.getLogger(Examples.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
	}

	static public void CallBack_Example() {
		/*
		 * In order to make this example more clear, both creation and call of
		 * the callback are made in the same scope. Usually, callback is created
		 * in the scope of where callback is supposed to run and then sent as a
		 * parameter to another object. At the certain point, object with
		 * pre-set callback will execute callback's action. It might be, for
		 * example, just before closing a window.
		 */
		CallBack cb = new CallBack() {
			@Override
			public void callBackAction(Object... parameters) {
				if (parameters.length == 0) {
					System.out.println("Example of a callback.");
				} else if (parameters[0] instanceof Integer) {
					final Integer integer = (Integer) parameters[0];
					System.out.println("Example of a callback with integer parameter: " + integer.intValue());
				} else if (parameters[0] instanceof String) {
					final String string = (String) parameters[0];
					System.out.println("Example of a callback with string parameter: " + string);
				} else {
					System.out.println("Example of a callback with unknown parameter.");
				}
			}
		};

		cb.callBackAction();
		cb.callBackAction(1);
		cb.callBackAction("Hello, World!");
		cb.callBackAction(10.0);
	}
}
