package com.elecfant.lib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Map<K, V> implements Iterable<V>{

	private List<K> keys;
	private List<V> values;

	public Map() {
		keys = new ArrayList<K>();
		values = new ArrayList<V>();
	}

	public void put(K key, V value) {
		if (key == null)
			return;
		int i = 0;
		for (K k : keys) {
			if (k != null && k.equals(key)) {
				values.set(i, value);
				return;
			}
			++i;
		}
		keys.add(key);
		values.add(value);
	}

	public V get(K key) {
		if (key == null)
			return null;
		int i = 0;
		for (K k : keys) {
			if (k != null && k.equals(key)) {
				return values.get(i);
			}
			++i;
		}
		return null;
	}

	public int size() {
		if (values.size() != keys.size()) {
			return -1;
		} else {
			return values.size();
		}
	}

	/**
	 * Practically only a shorthand for checking if "key" is present. Calls "get(K key)" method and checks if returned null.
	 * @param key
	 * @return
	 */
	public boolean containsKey(K key) {
		return get(key) != null;
	}

	@Override
	public Iterator<V> iterator() {
		return values.iterator();
	}
}
