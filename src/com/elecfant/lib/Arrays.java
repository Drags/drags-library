/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elecfant.lib;


/**
 *
 * @author Drags
 */
public class Arrays {
  static public Object[] concatenateArrays(Object[] source,Object[] target) {
    // safety checks
    if (source.getClass().isInstance(target)) {
      System.out.println("ERROR: Concatenation of incompatible class objects.");
      return target;
    }
    if (source == null || source.length == 0) {
      System.out.println("WARNING: Trying to concatenate empty or null array. Skipping");
      return target;
    }
    if (target == null) {
      System.out.println("WARNING: Concatenation to null array. Target array was replaced by source.");
      target = source;
      return target;
    }
    Object[] temp = new Object[target.length+source.length];
    System.arraycopy(target, 0, temp, 0, target.length);
    System.arraycopy(source, 0, temp, target.length, source.length);
    target = temp;
    return target;
  }
}
