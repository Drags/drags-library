package com.elecfant.lib;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Drags
 */
public class IOMethods {

  static public String readFileAsString(String filePath) throws IOException {
    StringBuilder fileData = new StringBuilder();
    try (BufferedReader reader = new BufferedReader(
                    new FileReader(filePath))) {
      char[] buf = new char[1024];
      int numRead;
      while ((numRead = reader.read(buf)) != -1) {
        String readData = String.valueOf(buf, 0, numRead);
        fileData.append(readData);
      }
    }
    return fileData.toString();
  }
}
